package hu.webvalto.practices.jpa.stockmarket.services;

import hu.webvalto.practices.jpa.stockmarket.daos.BaseDao;
import hu.webvalto.practices.jpa.stockmarket.daos.TradeDao;
import hu.webvalto.practices.jpa.stockmarket.entities.PortfolioElement;
import hu.webvalto.practices.jpa.stockmarket.entities.Trade;
import hu.webvalto.practices.jpa.stockmarket.entities.TradeStatusType;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Named
@Transactional
public class TradeManagerService {

    @Inject
    private TradeDao tradeDao;

    @Inject
    private BaseDao baseDao;

    public void handleNewTrade(Trade trade) {
        baseDao.save(trade);
        handleTradeExpirations();
        tradeDao.findSellingTrades().forEach(this::handleSellingTrade);
    }

    private void handleTradeExpirations() {
        tradeDao.findExpiringTrades().forEach(t -> t.setStatus(TradeStatusType.EXPIRED));
    }

    private void handleSellingTrade(Trade sellingTrade) {
        List<Trade> applicableBuyingTrades = tradeDao.findBuyingTradesByPriceAndVolumeLimit(sellingTrade.getPrice(), sellingTrade.getVolume(), sellingTrade.getCompany());

        long volume = sellingTrade.getVolume();
        for (Trade applicableBuyingTrade : applicableBuyingTrades) {
            if (volume >= applicableBuyingTrade.getVolume()) {
                volume -= applicableBuyingTrade.getVolume();
                applicableBuyingTrade.setStatus(TradeStatusType.CLOSED);
                registerBuyInPortfolio(applicableBuyingTrade);
                registerSellInPortfolio(sellingTrade, applicableBuyingTrade);
                if (volume == 0L) {
                    break;
                }
            }
        }
        if (volume > 0L) {
            sellingTrade.setVolume(volume);
        } else {
            sellingTrade.setStatus(TradeStatusType.CLOSED);
        }
    }

    private void registerSellInPortfolio(Trade sellingTrade, Trade applicableBuyingTrade) {
        sellingTrade.getCompany().getPortfolio().stream()
                .filter(pe -> pe.getStock().equals(sellingTrade.getStock()))
                .forEach(pe ->
                        pe.setVolume(pe.getVolume() - applicableBuyingTrade.getVolume())
                );
    }

    private void registerBuyInPortfolio(Trade applicableBuyingTrade) {
        if (applicableBuyingTrade.getCompany().getPortfolio().stream().noneMatch(portfolioElement -> portfolioElement.getStock().equals(applicableBuyingTrade.getStock()))) {
            PortfolioElement portfolioElement = PortfolioElement.builder()
                    .id(UUID.randomUUID().toString())
                    .volume(applicableBuyingTrade.getVolume())
                    .company(applicableBuyingTrade.getCompany())
                    .stock(applicableBuyingTrade.getStock())
                    .build();
            baseDao.save(portfolioElement);
        } else {
            applicableBuyingTrade.getCompany().getPortfolio().stream()
                    .filter(pe -> pe.getStock().equals(applicableBuyingTrade.getStock()))
                    .forEach(pe ->
                            pe.setVolume(pe.getVolume() + applicableBuyingTrade.getVolume())
                    );
        }
    }

}
