package hu.webvalto.practices.jpa.stockmarket.services;

import hu.webvalto.practices.jpa.stockmarket.daos.BaseDao;
import hu.webvalto.practices.jpa.stockmarket.entities.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Named
public class SellServiceImpl implements SellService {

    @Inject
    private BaseDao baseDao;

    @Inject
    private TradeManagerService tradeManagerService;

    @Override
    public void makeTrade(Long volume, Double price, Stock stock) {
        if (hasEnoughStock(volume, stock)) {
            Trade trade = Trade.builder()
                    .id(UUID.randomUUID().toString())
                    .company(getLoggedBrokerCompany())
                    .stock(stock)
                    .price(price)
                    .expires(getExpireDate())
                    .tradeType(TradeType.SELL)
                    .volume(volume)
                    .status(TradeStatusType.ACTIVE)
                    .build();
            tradeManagerService.handleNewTrade(trade);
        } else {
            throw new IllegalStateException("Your company haven't got enough stock for this trade");
        }
    }

    private boolean hasEnoughStock(Long volume, Stock stock) {
        //  fetch = FetchType.EAGER akkor működik a stream-es megoldás is.

        for (PortfolioElement portfolioElement : getLoggedBrokerCompany().getPortfolio()) {
            if (portfolioElement.getStock().equals(stock) && portfolioElement.getVolume() >= volume) {
                return true;
            }
        }
        return false;
    }

}
