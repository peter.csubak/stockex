package hu.webvalto.practices.jpa.stockmarket.services;

import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import hu.webvalto.practices.jpa.stockmarket.entities.Company;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import hu.webvalto.practices.jpa.stockmarket.ui.VaadinUI;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static java.time.temporal.ChronoUnit.*;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public interface TradeService {

    void makeTrade(Long volume, Double price, Stock stock);

    default Company getLoggedBrokerCompany() {
        return ((VaadinUI) VaadinUI.getCurrent()).getLoggedInBroker().getCompany();
    }

    default Broker getLoggedBroker() {
        return ((VaadinUI) VaadinUI.getCurrent()).getLoggedInBroker();
    }

    default Date getExpireDate() {
        return Date.from(LocalDateTime.now().plus(10, MINUTES).atZone(ZoneId.systemDefault()).toInstant());
    }
}
