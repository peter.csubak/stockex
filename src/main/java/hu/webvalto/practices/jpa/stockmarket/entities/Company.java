package hu.webvalto.practices.jpa.stockmarket.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Cacheable(value = false)
@Table(name = "COMPANIES")
public class Company {

    @Id
    private String id = UUID.randomUUID().toString();

    private String name;

    @Lob
    private String details;

    @OneToMany(mappedBy = "company", fetch = FetchType.EAGER)
    private List<PortfolioElement> portfolio;

    public String toString() {
        return name;
    }
}
