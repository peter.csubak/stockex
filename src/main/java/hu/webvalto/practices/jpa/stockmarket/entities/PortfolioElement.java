package hu.webvalto.practices.jpa.stockmarket.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Table(name = "PORTFOLIO")
@Cacheable(value = false)
public class PortfolioElement {

    @Id
    private String id = UUID.randomUUID().toString();

    private Long volume;

    @ManyToOne
    @JoinColumn(name = "COMPANY_ID")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "STOCK_ID")
    private Stock stock;

    @ManyToMany
    @JoinTable(name = "PORTFOLIO_BROKER_JT",
            joinColumns = @JoinColumn(name = "BROKER_ID"),
            inverseJoinColumns = @JoinColumn(name = "PORTFOLIO_ID")
    )
    private List<Broker> portfolioManagers;

}
