package hu.webvalto.practices.jpa.stockmarket.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor()
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "BROKERS")
public class Broker {

    @Id
    private String id = UUID.randomUUID().toString();

    private String name;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "COMPANY_ID")
    private Company company;

    @ManyToMany(mappedBy = "portfolioManagers")
    private List<PortfolioElement> managedPortfolioElements;

}
