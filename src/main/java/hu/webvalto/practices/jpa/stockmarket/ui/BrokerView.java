package hu.webvalto.practices.jpa.stockmarket.ui;

import com.vaadin.cdi.CDIView;
import com.vaadin.cdi.UIScoped;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.CssLayout;
import hu.webvalto.practices.jpa.stockmarket.daos.BaseDao;
import hu.webvalto.practices.jpa.stockmarket.entities.Broker;
import org.vaadin.cdiviewmenu.ViewMenuItem;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.fields.MTable;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@UIScoped
@CDIView("Broker")
@ViewMenuItem(order = 5, icon = FontAwesome.BRIEFCASE, title = "Brokers")
public class BrokerView extends CssLayout implements View {

    @Inject
    private BaseDao baseDao;

    private MTable<Broker> brokerTable = new MTable<>(Broker.class)
            .withFullWidth()
            .withHeight("450px")
            .withProperties("name",
                    "company.name")
            .withColumnHeaders("Name", "Company");

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        refreshTable();
    }

    @PostConstruct
    private void init() {
        addComponents(
                new MVerticalLayout(
                        new MHorizontalLayout(brokerTable).withFullWidth()
                ).withSize(MSize.FULL_SIZE)
        );
        setSizeFull();
        refreshTable();
    }

    private void refreshTable() {
        brokerTable.setBeans(baseDao.findAll(Broker.class));
    }
}
