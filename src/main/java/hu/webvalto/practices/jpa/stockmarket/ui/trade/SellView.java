package hu.webvalto.practices.jpa.stockmarket.ui.trade;

import com.vaadin.cdi.CDIView;
import com.vaadin.cdi.UIScoped;
import com.vaadin.server.FontAwesome;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import hu.webvalto.practices.jpa.stockmarket.services.SellService;
import org.vaadin.cdiviewmenu.ViewMenuItem;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
@UIScoped
@CDIView("Sell")
@ViewMenuItem(order = 3, icon = FontAwesome.ARROW_RIGHT, title = "Sell")
public class SellView extends TradeViewDefinition {

    @Inject
    private SellService sellService;

    @Override
    protected void makeTrade() {
        sellService.makeTrade(getVolumeValue(), getPriceValue(), getSelectedStock());
    }

    @Override
    protected List<Stock> getStocks() {
        return tradeDaoService.findStocksForSelling(getLoggedInBroker());
    }
}
