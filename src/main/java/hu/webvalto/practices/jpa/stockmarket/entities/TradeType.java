package hu.webvalto.practices.jpa.stockmarket.entities;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
public enum TradeType {

    SELL, BUY

}
