package hu.webvalto.practices.jpa.stockmarket.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Data
@Entity
@Table(name = "PRICE_HISTORY")
public class PriceHistoryElement {

    @Id
    private String id = UUID.randomUUID().toString();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CHANGE_TIMESTAMP")
    private Date changeTimestamp;

    private Double price;

    @ManyToOne
    @JoinColumn(name = "STOCK_ID")
    private Stock stock;

}
