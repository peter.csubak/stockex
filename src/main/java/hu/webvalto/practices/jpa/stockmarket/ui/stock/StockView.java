package hu.webvalto.practices.jpa.stockmarket.ui.stock;

import com.vaadin.cdi.CDIView;
import com.vaadin.cdi.UIScoped;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import hu.webvalto.practices.jpa.stockmarket.daos.BaseDao;
import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import org.vaadin.cdiviewmenu.ViewMenuItem;
import org.vaadin.viritin.MSize;
import org.vaadin.viritin.fields.MTable;
import org.vaadin.viritin.fields.MValueChangeEvent;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@UIScoped
@CDIView("Main")
@ViewMenuItem(order = 1, icon = FontAwesome.LIST, title = "Stocks")
public class StockView extends CssLayout implements View {

    @Inject
    private BaseDao baseDao;

    @Inject
    private StockEditorForm stockForm;

    private MTable<Stock> stockList = new MTable<>(Stock.class)
            .withFullWidth()
            .withHeight("450px")
            .withProperties("name", "symbol", "price")
            .withColumnHeaders("Name", "Symbol", "Price");

    private Button newButton = new Button("New", this::editNew);
    private Button editButton = new Button("Edit", this::edit);


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        refreshList();
    }

    private void listValueChange(MValueChangeEvent<Stock> event) {
        Stock stock = event.getValue();
        editButton.setEnabled(stock != null);

        if (stock != null) {
            stockForm.setVisible(false);
        }
    }

    private void edit(Button.ClickEvent event) {
        editStock(stockList.getValue());
    }

    private void editStock(Stock stock) {
        if (stock != null) {
            stockForm.setVisible(true);
            stockForm.setEntity(stock);
            stockForm.focusFirst();
        }
    }

    private void editNew(Button.ClickEvent event) {
        editStock(new Stock());
    }

    @PostConstruct
    private void init() {
        stockForm.setSavedHandler(this::saveStock);

        editButton.setEnabled(false);
        stockList.addMValueChangeListener(this::listValueChange);
        addComponents(
                new MVerticalLayout(
                        new MHorizontalLayout(newButton, editButton),
                        new MHorizontalLayout(stockList).withFullWidth(),
                        stockForm
                ).withSize(MSize.FULL_SIZE)
        );

        setWidth(100f, Unit.PERCENTAGE);
        stockForm.setVisible(false);
        refreshList();
    }

    private void refreshList() {
        stockList.setBeans(baseDao.findAll(Stock.class));
    }

    private void saveStock(Stock stock) {
        baseDao.save(stock);
        stockForm.setVisible(false);
        stockList.setValue(null);
        refreshList();
    }
}
