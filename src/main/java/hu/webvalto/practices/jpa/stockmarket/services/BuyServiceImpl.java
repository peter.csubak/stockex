package hu.webvalto.practices.jpa.stockmarket.services;

import hu.webvalto.practices.jpa.stockmarket.entities.Stock;
import hu.webvalto.practices.jpa.stockmarket.entities.Trade;
import hu.webvalto.practices.jpa.stockmarket.entities.TradeStatusType;
import hu.webvalto.practices.jpa.stockmarket.entities.TradeType;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.UUID;

/**
 * @author Péter Csubák <peter.csubak@webvalto.hu>
 */
@Named
@Transactional
public class BuyServiceImpl implements BuyService {

    @Inject
    private TradeManagerService tradeManagerService;

    @Override
    public void makeTrade(Long volume, Double price, Stock stock) {
        Trade trade = Trade.builder()
                .id(UUID.randomUUID().toString())
                .company(getLoggedBrokerCompany())
                .stock(stock)
                .price(price)
                .expires(getExpireDate())
                .tradeType(TradeType.BUY)
                .volume(volume)
                .status(TradeStatusType.ACTIVE)
                .build();
        tradeManagerService.handleNewTrade(trade);
    }

}
